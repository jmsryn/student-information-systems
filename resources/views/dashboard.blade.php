@extends('layout.boiler')
@section('body')
    <div class="main">
        <div class="header">
            <h1>Dashboard</h1>
        </div>
        <div class="add">
            <a href="/add"><span><i class="fas fa-plus-square"></i></span>  ADD STUDENT</a>
        </div>
        <div class="header2">
            <h2>Student Information</h2>
        </div>
        <div class="std_table">
            <table>
                <thead>
                    <th>Student ID</th>
                    <th>Fullname</th>
                    <th>Course</th>
                    <th>Year Level</th>
                    <th>Date of Birth</th>
                    <th>Address</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                        <tr>
                            <td>{{ $item->studentID }}</td>
                            <td>{{ $item->first_name . ' ' .$item->middle_name . ' ' . $item->last_name}}</td>
                            <td>{{ $item->course }}</td>
                            <td>{{ $item->year_level }}</td>
                            <td>{{ $item->date_of_birth }}</td>
                            <td>{{ $item->home_address }}</td>
                            <td>
                                <a href="/edit/{{ $item->studentID }}" style="background-color:  #64DD17"><span><i class="fas fa-edit"></i></span> Edit</a>
                                <a href="/delete/{{ $item->studentID }}" style="background-color: #DD2C00"><span><i class="fas fa-trash-alt"></i></span> Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>   
@endsection