<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [StudentsController::class, 'dashboard']);
Route::get('/add', [StudentsController::class, 'add']);
Route::post('/add', [StudentsController::class, 'store']);
Route::get('/delete/{id}', [StudentsController::class, 'delete']);
Route::get('/edit/{id}', [StudentsController::class, 'edit']);
Route::post('/edit/{id}', [StudentsController::class, 'update']);

